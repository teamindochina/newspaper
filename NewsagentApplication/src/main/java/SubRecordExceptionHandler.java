

public class SubRecordExceptionHandler extends Exception {
	String message;
	
	public SubRecordExceptionHandler(String errMessage){
		message = errMessage;
	}
	
	public String getMessage() {
		return message;
	}
}
