

public class Delivery {
    int id;
    String areaName;
    String streetName;
    String houseNumber;
    int date;
    public Delivery(int id,String areaName,String streetName,String houseNumber,int date) {
    	  try{ 
    		    checkId(id);
    	    	checkAddressName(areaName);
    	    	checkAddressName(streetName);
    		  } catch(DeliveryFormatException e) {
    		 e.printStackTrace();
    	  }
        this.id = id;
        this.areaName = areaName;
        this.streetName = streetName;
        this.houseNumber = houseNumber;
        this.date = date;
    }
      public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }
    public static void checkId(int id) throws DeliveryFormatException {
        if (id < 100 || id > 999999) {
            throw new DeliveryFormatException("Delivery ID contains 3 to 6 valid characters between 0 to 9.");
        }
    }
      public static void checkDate(int date) throws DeliveryFormatException {
        if (date<1 || date>30) {
            throw new DeliveryFormatException("Delivery date is invalid");
        }
        }

    public static void checkAddressName(String addressName) throws DeliveryFormatException {
        if (addressName.length() < 3 || !addressName.matches("[a-zA-Z]+")) {
            throw new DeliveryFormatException("Area name contains 4 to 16 valid characters between a to z.");
        }
    }


   
}
