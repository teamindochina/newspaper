

public class SubRecord {
	private int subId;
	private String subName;
	private int subQty;
	private int deliFreq;
	private int cusId;
	private String subDate;
	
	public void setSubId(int subid) {
		subId=subid;
	}
	public void setSubName(String subname) {
		subName=subname;
	}
	public void setSubQty(int subqty) {
		subQty=subqty;
	}
	public void setDeliFreq(int delifreq) {
		deliFreq=delifreq;
	}
	public void setCusId(int cusid) {
		cusId=cusid;
	}
	public void setSubDate(String subdate) {
		subDate=subdate;
	}
	
	public int getSubId() {
		return subId;
	}
	public String getSubName() {
		return subName;
	}
	public int getSubQty() {
		return subQty;
	}
	public int getDeliFreq() {
		return deliFreq;
	}
	public int getCusId() {
		return cusId;
	}
	public String getSubDate() {
		return subDate;
	}
	
	public SubRecord(int subid, String subname, int subqty, int delifreq, int cusid, String subdate) throws SubRecordExceptionHandler {
		// TODO Auto-generated constructor stub
		
		subId = subid;
		subName = subname;
		subQty = subqty; 
		deliFreq = delifreq;
		cusId = cusid;
		subDate = subdate;
		// Validate Input
		try {
			validateSubId(subId);
			validateSubName(subName);
			validateSubQty(subQty);
			validateDeliFreq(deliFreq);
			validateCusId(cusId);
		}
		catch (SubRecordExceptionHandler e) {
			throw e;
		}
		
		
	}
	
	
	public static void validateSubId(int Id) throws SubRecordExceptionHandler {
		int id_len = Integer.toString(Id).length();
		
		if(id_len<4) {
			throw new SubRecordExceptionHandler("id less than 4 characters");
			}
		else if(id_len>8) {
			throw new SubRecordExceptionHandler("id more than 8 characters");
			}
	}
	
	public static void validateSubName(String Name) throws SubRecordExceptionHandler {
		boolean letter_res = true;
		int name_len = Name.length();
		char[] chars = Name.toCharArray();
		for (char c : chars) {
			if(!Character.isLetter(c)) letter_res = false;
		}
		
		if(name_len<3) 
			throw new SubRecordExceptionHandler("name less than 3 characters");
		else if(name_len>16) 
			throw new SubRecordExceptionHandler("name more than 16 characters");
		else {
			if(!letter_res) 
				throw new SubRecordExceptionHandler("name including number or other special characters");
		}
	}
	
	public static void validateSubQty(int qty) throws SubRecordExceptionHandler {
		
		if(qty<0) {
			throw new SubRecordExceptionHandler("qty less than 0");
			}
	}
	
	public static void validateDeliFreq(int freq) throws SubRecordExceptionHandler {
		
		if(freq<1) {
			throw new SubRecordExceptionHandler("freq less than 1");
			}
	}
	public static void validateCusId(int c) throws SubRecordExceptionHandler {
		
		if(c<1) {
			throw new SubRecordExceptionHandler("cusId less than 1");
			}
	}
	
	
	
}
