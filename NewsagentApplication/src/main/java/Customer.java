

public class Customer {

    private int id;
    private String name;
    private String phone;
    private String password;

    public Customer(String custname, String custphone, String custpassword) throws CustomerExceptionHandler {
        id = 0;
        // Validate Input
        validatename(custname);
        validatephone(custphone);
        validatepassword(custpassword);
        // Set Attributes
        name = custname;
        phone = custphone;
        password = custpassword;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static void validatename(String custfname) throws CustomerExceptionHandler {

        //Agree Formating Rules on "Customer Name"
        //E.G. Name String must be a minimum of 2 characters and a maximum of 50 characters

        if (custfname.isBlank() || custfname.isEmpty())
            throw new CustomerExceptionHandler("Customer Name NOT specified");
        else if (custfname.length() < 2)
            throw new CustomerExceptionHandler("Customer Name does not meet minimum length requirements");
        else if (custfname.length() > 20)
            throw new CustomerExceptionHandler("Customer Name does not exceeds maximum length requirements");

    }
	/*
	public static void validatelname(String custlname) throws main.exceptions.CustomerExceptionHandler {

		//Agree Formating Rules on "Customer Address"
		//E.G. Name String must be a minimum of 5 characters and a maximum of 60 characters

		if (custlname.isBlank() || custlname.isEmpty())
			throw new main.exceptions.CustomerExceptionHandler("Customer Address NOT specified");
		else if (custlname.length() < 5)
			throw new main.exceptions.CustomerExceptionHandler("Customer Address does not meet minimum length requirements");
		else if (custlname.length() > 20)
			throw new main.exceptions.CustomerExceptionHandler("Customer Address does not exceeds maximum length requirements");

	}*/

    public static void validatephone(String custphone) throws CustomerExceptionHandler {

        //Agree Formating Rules on "Customer PhoneNumber"
        //E.G. Name String must be a minimum of 7 characters and a maximum of 15 characters

        if (custphone.isBlank() || custphone.isEmpty())
            throw new CustomerExceptionHandler("Customer PhoneNumber NOT specified");
        else if (custphone.length() < 7)
            throw new CustomerExceptionHandler("Customer PhoneNumber does not meet minimum length requirements");
        else if (custphone.length() > 15)
            throw new CustomerExceptionHandler("Customer PhoneNumber does not exceeds maximum length requirements");

    }

    public static void validatepassword(String custpassword) throws CustomerExceptionHandler {
        if (custpassword.isBlank() || custpassword.isEmpty())
            throw new CustomerExceptionHandler("Customer Password NOT specified");
        else if (custpassword.length() < 8)
            throw new CustomerExceptionHandler("Customer Password does not meet minimum length requirements");
        else if (custpassword.length() > 20)
            throw new CustomerExceptionHandler("Customer Password does not exceeds maximum length requirements");

    }

}


