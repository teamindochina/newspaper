import java.sql.ResultSet;
import java.util.Scanner;

public class CustomerOperation {

    MySQLAccess dao;

    public CustomerOperation() throws Exception {
        dao = new MySQLAccess();
    }

    private static void listCustomerMenu() {
        System.out.println("Choose any one of the following");
        System.out.println("1. Create a customer record");
        System.out.println("2. View all customer records");
        System.out.println("3. Delete a customer record");
        System.out.println("4. Update a customer record");

    }

    public boolean performOperation(Scanner keyboard) throws Exception {
        listCustomerMenu();
        String operationNumber = keyboard.nextLine();
        switch (operationNumber) {
            case "1":
            	Customer custObj =null;
                //Get Customer Details from the User
            	try {
                System.out.printf("Enter Customer Name: \n");
                String custname = keyboard.next();
                System.out.printf("Enter Customer PhoneNumber: \n");
                String custphoneNumber = keyboard.next();
                System.out.printf("Enter Customer Password: \n");
                String custpassword = keyboard.next();
                custObj = new Customer(custname, custphoneNumber, custpassword);
            	}catch(Exception e) {
            		System.out.println("invalid input");
            		break;
            	}
                //Insert Customer Details into the database
                boolean insertResult = dao.insertCustomerDetailsAccount(custObj);
                System.out.println("here");
                if (insertResult == true)
                    System.out.println("Customer Details Saved");
                else
                    System.out.println("ERROR: Customer Details NOT Saved");
                break;

            case "2":
                //Retrieve ALL Customer Records
                ResultSet rSet = dao.retrieveAllCustomerAccounts();
                if (rSet == null) {
                    System.out.println("No Records Found");
                    break;
                }
                boolean tablePrinted = printCustomerTable(rSet);
                if (tablePrinted)
                    rSet.close();
                break;

            case "3":
                //Delete Customer Record by ID
               try { System.out.println("Enter Customer Id to be deleted or -99 to Clear all Rows");
                String deleteCustId = keyboard.next();
                boolean deleteResult = dao.deleteCustomerById(Integer.parseInt(deleteCustId));
                if ((deleteResult == true) && (deleteCustId.equals("-99")))
                    System.out.println("Customer Table Emptied");
                   
                else if (deleteResult == true)
                    System.out.println("Customer Deleted");
                else
                    System.out.println("Customer Details NOT Deleted or Do Not Exist");}
               catch(Exception e) {
            	   System.out.println("invalid input");
            	   break;
               }
                 
                
            case "4":
            	try {
                System.out.println("Enter Customer Id to be Updated");
                int modifyCustId = keyboard.nextInt();
                System.out.println("Choose one of options to modify");
                System.out.println("1.Customer name");
                System.out.println("2.Customer phone");
                System.out.println("3.Customer password");
                int newChange = keyboard.nextInt();
                System.out.println("Enter new content to update");
                String newContent = keyboard.next();
                boolean modifyResult = dao.modifyCustomerById(modifyCustId, newChange, newContent);
                if (modifyResult == true)
                    System.out.println("Subscription Table Updated");
                else
                    System.out.println("ERROR: Subscriptions NOT Updated or Do Not Exist");}catch(Exception e) {
                    	System.out.println("Invalid input"); 
                    	break;
                    }
               
            case "99":
                System.out.println("Closing the Application");
                return false;
            default:
                System.out.println("No Valid Function Selected");
                break;
        }
        return true;
    }

    private static boolean printCustomerTable(ResultSet rs) throws Exception {

        //Print The Contents of the Full Customer Table<asf>

        System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Table: " + rs.getMetaData().getTableName(1));
        for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
            System.out.printf("%30s", rs.getMetaData().getColumnName(i));
        }
        System.out.println();
        while (rs.next()) {
            System.out.println();
            int id = rs.getInt("customer_id");
            String custname = rs.getString("customer_name");
            String phone = rs.getString("customer_phone");
            String password = rs.getString("customer_pwd");
            System.out.printf("%30s", id);
            System.out.printf("%30s", custname);
            System.out.printf("%30s", phone);
            System.out.printf("%30s", password);
            System.out.println();
        }
        return false;
    }
}
