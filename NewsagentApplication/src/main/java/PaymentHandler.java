

public class PaymentHandler {
	//Define vars
	private int id;
	private String method;
	
	//Define setters
	void setID(int payID) {
		id = payID;
	}
	
	void setMethod(String payMethod) {
		method = payMethod;
	}
	
	//Define getters
	public int getID() {
		return id;
	}
	
	public String getMethod() {
		return method;
	}
	
	//Define constructor
	public PaymentHandler(int payID, String payMethod) throws PaymentHandlerExceptionHandler {
		//id = 0; //auto increment in DB
		
		//validate inputs
		try {
			validateID(payID);
			validateMethod(payMethod);
		}
		catch(PaymentHandlerExceptionHandler e) {
			throw e;
		}
		
		id = payID;
		method = payMethod;
	}
	
    public static void validateID(int payID) throws PaymentHandlerExceptionHandler{
		//throw new RuntimeException("No code written");
    	if(payID<100)
    		throw new PaymentHandlerExceptionHandler("Payment ID below expected range");
    	else if (payID>999999)
    		throw new PaymentHandlerExceptionHandler("Payment ID above expected range");
	}
    
    public static void validateMethod(String payMethod) throws PaymentHandlerExceptionHandler{
    	//throw new RuntimeException("No code written");
    	if (payMethod.isBlank() || payMethod.isEmpty())
    		throw new PaymentHandlerExceptionHandler("No input received for Payment method");
    	else if(!"Cash".equals(payMethod) & !"Card".equals(payMethod))
    		throw new PaymentHandlerExceptionHandler("Input not in either expected category");
    	/*else if(!"Card".equals(payMethod))
    		throw new main.exceptions.PaymentHandlerExceptionHandler("Input not in either expected category");
    	else if(!"Cash".equals(payMethod))
    		throw new main.exceptions.PaymentHandlerExceptionHandler("Input not in either expected category");*/
	}
	
}
