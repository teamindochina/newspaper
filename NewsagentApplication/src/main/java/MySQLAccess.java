

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.sql.ResultSet;

public class MySQLAccess {
	private final Connection connect;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;

	public MySQLAccess() throws Exception{
		// Load the JConnector Driver
		Class.forName("com.mysql.cj.jdbc.Driver");
		// Specify the DB Name
		String url="jdbc:mysql://localhost:3306/news_sys?serverTimezone=UTC";
		// Connect to DB using DB URL, Username and password
		connect = DriverManager.getConnection(url, "root", "649Yuchen");

	}

	//Add your own methods for CRUD
	//----------------Payment-------------------------------------------
	public boolean insertNewPaymentDetail(PaymentHandler p) {
		boolean insertSuccessful = true;

		try {
			preparedStatement = connect.prepareStatement("INSERT INTO news_sys.payment(customer_id, payment_method) VALUES (?, ?)");
			preparedStatement.setInt(1, p.getID());
			preparedStatement.setString(2, p.getMethod());
			preparedStatement.executeUpdate();
		}
		catch(Exception e) {
			insertSuccessful = false;
		}

		return insertSuccessful;
	}

	public ResultSet displayAllPaymentDetails() {
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT * FROM news_sys.payment");
		}
		catch(Exception e) {
			resultSet = null;
		}
		return resultSet;
	}

	public boolean updateExistingPaymentDetail(PaymentHandler p) {
		boolean updateSuccessful = true;

		try {
			preparedStatement = connect.prepareStatement("UPDATE news_sys.payment SET payment_method = ? WHERE customer_id = ?");
			preparedStatement.setInt(2, p.getID());
			preparedStatement.setString(1, p.getMethod());
			preparedStatement.executeUpdate();
		}
		catch(Exception e) {
			updateSuccessful = false;
		}

		return updateSuccessful;
	}

	public boolean deletePaymentByID(int payID) {
		boolean deleteSuccessfull = true;
		try {
			if(payID == -99)
				preparedStatement = connect.prepareStatement("DELETE FROM news_sys.payment");
			else
				preparedStatement = connect.prepareStatement("DELETE FROM news_sys.payment WHERE customer_id = " + payID);
			preparedStatement.executeUpdate();
		}
		catch(Exception e) {
			deleteSuccessfull = false;
		}
		return deleteSuccessfull;
	}

	public boolean insertNewPaymentReminder(PaymentReminder r) {
		boolean insertSuccessful = true;

		try {
			preparedStatement = connect.prepareStatement("INSERT INTO news_sys.PAY_REMINDER(CUSTOMER_ID, AMOUNT_DUE, DATE_DUE) VALUES (?, ?, ?)");
			preparedStatement.setInt(1, r.getID());
			preparedStatement.setDouble(2, r.getAmount());
			preparedStatement.setDate(3, java.sql.Date.valueOf(r.getDate()));
			preparedStatement.executeUpdate();
		}
		catch(Exception e) {
			insertSuccessful = false;
		}

		return insertSuccessful;
	}

	public ResultSet displayAllPaymentReminders() {
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT * FROM news_sys.pay_reminder");
		}
		catch(Exception e) {
			resultSet = null;
		}
		return resultSet;
	}

	public boolean updateExistingPaymentReminder(PaymentReminder r) {
		boolean updateSuccessful = true;

		try {
			preparedStatement = connect.prepareStatement("UPDATE news_sys.PAY_REMINDER SET AMOUNT_DUE = ?, DATE_DUE = ? WHERE CUSTOMER_ID = ?)");

			preparedStatement.setDouble(1, r.getAmount());
			preparedStatement.setDate(2, java.sql.Date.valueOf(r.getDate()));
			preparedStatement.setInt(3, r.getID());
			preparedStatement.executeUpdate();
		}
		catch(Exception e) {
			updateSuccessful = false;
		}

		return updateSuccessful;
	}

	public boolean deletePaymentReminderByID(int reminderID) {
		boolean deleteSuccessfull = true;
		try {
			if(reminderID == -99)
				preparedStatement = connect.prepareStatement("DELETE FROM news_sys.PAY_REMINDER");
			else
				preparedStatement = connect.prepareStatement("DELETE FROM news_sys.PAY_REMINDER WHERE CUSTOMER_ID = " + reminderID);
			preparedStatement.executeUpdate();
		}
		catch(Exception e) {
			deleteSuccessfull = false;
		}
		return deleteSuccessfull;
	}

	//----------------Subscription-------------------------------------------
	public boolean insertSubscriptionDetails(SubRecord s) {

		boolean insertSucessfull = true;

		//Add Code here to call embedded SQL to insert Customer into DB

		try {

			//Create prepared statement to issue SQL query to the database
			preparedStatement = connect.prepareStatement("insert into news_sys.subscription_details values (?, ?, ?, ?, ?, ?)");
			preparedStatement.setInt(1, s.getSubId());
			preparedStatement.setString(2, s.getSubName());
			preparedStatement.setInt(3, s.getSubQty());
			preparedStatement.setInt(4, s.getDeliFreq());
			preparedStatement.setInt(5, s.getCusId());
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Date d = df.parse(s.getSubDate());
			java.sql.Date dd = new java.sql.Date(d.getTime());
			preparedStatement.setDate(6, dd);
			preparedStatement.executeUpdate();
		}
		catch (Exception e) {
			insertSucessfull = false;
		}

		return insertSucessfull;

	}// end insertCustomerDetailsAccount

	public ResultSet retrieveAllSubscriptions() {

		//Add Code here to call embedded SQL to view Customer Details

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from news_sys.subscription_details");

		}
		catch (Exception e) {
			resultSet = null;
		}
		return resultSet;
	}

	public boolean deleteSubscriptinById(int subId) {

		boolean deleteSucessfull = true;

		//Add Code here to call embedded SQL to insert Customer into DB

		try {

			//Create prepared statement to issue SQL query to the database
			//if (subId == -99)
				//Delete all entries in Table
				//preparedStatement = connect.prepareStatement("delete from news_sys.subscription_details");
			//else
				//Delete a particular Customer
			preparedStatement = connect.prepareStatement("delete from news_sys.subscription_details where sub_id = " + subId);
			preparedStatement.executeUpdate();

		}
		catch (Exception e) {
			deleteSucessfull = false;
		}

		return deleteSucessfull;

	}

	public boolean modifySubscriptinById(int modifySubId, int newChange, String newContent) {
		// TODO Auto-generated method stub
		boolean modifySucessfull = true;
		try {
			if(newChange == 1) {
				preparedStatement = connect.prepareStatement("update news_sys.subscription_details set item_name = '" + newContent + "' where sub_id = '" + modifySubId + "';");
			}
			else if(newChange == 2) {
				preparedStatement = connect.prepareStatement("update news_sys.subscription_details set item_qty = '" + Integer.parseInt(newContent) + "' where sub_id = '" + modifySubId + "';");

			}
			else if(newChange == 3) {
				preparedStatement = connect.prepareStatement("update news_sys.subscription_details set delivery_frequency = '" + Integer.parseInt(newContent) + "' where sub_id = '" + modifySubId + "';");
			}
			preparedStatement.executeUpdate();

		}
		catch (Exception e) {
			modifySucessfull = false;
		}
		return modifySucessfull;
	}

	public ResultSet retrieveSubCountByItemName(String itemName) {

		//Add Code here to call embedded SQL to view Customer Details

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("select item_name, count(*) as count from news_sys.subscription_details where item_name = '" + itemName + "';");

		}
		catch (Exception e) {
			resultSet = null;
		}
		return resultSet;
	}
	public ResultSet retrieveSubCountByCusId(int cusId) {

		//Add Code here to call embedded SQL to view Customer Details

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("select * from news_sys.subscription_details where customer_id = " + cusId + ";");

		}
		catch (Exception e) {
			resultSet = null;
		}
		return resultSet;
	}
	public ResultSet retrieveCusBill(int cusId, int month) {
		
		
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("select * from news_sys.subscription_details where customer_id = '"+ cusId +"' and MONTH(sub_date)='"+ month + "';");
		
		}
		catch (Exception e) {
			resultSet = null;
		}
		return resultSet;
	}

//-----------------main.data.Delivery---------------------
		public boolean insertNewDeliveryDetail(Delivery d) {
		boolean insertSuccessfull = true;
		try {
			preparedStatement = connect.prepareStatement("INSERT INTO news_sys.delivery VALUES (?, ?,?,?,?)");
			preparedStatement.setInt(1, d.getId());
			preparedStatement.setString(2, d.getAreaName());
			preparedStatement.setString(3, d.getStreetName());
			preparedStatement.setString(4, d.getHouseNumber());
			preparedStatement.setString(5, d.getDate()+"");
			preparedStatement.executeUpdate();
		}
		catch(Exception e) {
			insertSuccessfull = false;
			e.printStackTrace();
		}
		return insertSuccessfull;
	}
	public ResultSet displayAllDeliveryDetails() {
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT * FROM news_sys.delivery");
		}
		catch(Exception e) {
			resultSet = null;
		}
		return resultSet;
	}
	public boolean deleteDeliveryByID(int customerID) {
		boolean deleteSuccessfull = true;
		try {
			preparedStatement = connect.prepareStatement("DELETE FROM news_sys.delivery WHERE customer_id = " + customerID);
			preparedStatement.executeUpdate();
		}
		catch(Exception e) {
			deleteSuccessfull = false;
		}
		return deleteSuccessfull;
	}
	public ResultSet displayCustomersForEachArea() {
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT area_name,group_concat(customer_id) as customers FROM news_sys.delivery group by area_name");
		}
		catch(Exception e) {
			resultSet = null;
		}
		return resultSet;
	}
	public ResultSet displayitemstoday(int date) {
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT * FROM news_sys.delivery where delivery_date = "+ date);
		}
		catch(Exception e) {
			resultSet = null;
		}
		return resultSet;

	}
	//-----------------Customer---------------------
	public boolean insertCustomerDetailsAccount(Customer c) {
		boolean insertSucessfull = true;
		// Add Code here to call embedded SQL to insert Customer into DB
		try {
			// Create prepared statement to issue SQL query to the database
			preparedStatement = connect.prepareStatement("insert into news_sys.customers values (?, ?, ?, ?)");
			preparedStatement.setInt(1, c.getId());
			preparedStatement.setString(2, c.getName());
			preparedStatement.setString(3, c.getPhone());
			preparedStatement.setString(4, c.getPassword());
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			insertSucessfull = false;
		}

		return insertSucessfull;

	}// end insertCustomerDetailsAccount

	public ResultSet retrieveAllCustomerAccounts() {

		// Add Code here to call embedded SQL to view Customer Details

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from news_sys.customers");

		} catch (Exception e) {
			resultSet = null;
		}
		return resultSet;
	}

	public boolean deleteCustomerById(int custID) {

		boolean deleteSucessfull = true;

		// Add Code here to call embedded SQL to insert Customer into DB

		try {

			// Create prepared statement to issue SQL query to the database
			if (custID == -99)
				// Delete all entries in Table
				preparedStatement = connect.prepareStatement("delete from news_sys.customers");
			else
				// Delete a particular Customer
				preparedStatement = connect.prepareStatement("delete from news_sys.customers where customer_id = " + custID);
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			deleteSucessfull = false;
		}

		return deleteSucessfull;

	}


	public boolean modifyCustomerById(int modifyCustId, int newChange, String newContent) {
		// TODO Auto-generated method stub
		boolean modifySucessfull = true;
		try {
			if(newChange == 1) {
				preparedStatement = connect.prepareStatement("update news_sys.customers set customer_name = '" + newContent + "' where customer_id = '" + modifyCustId + "';");
			}
			else if(newChange == 2) {
				preparedStatement = connect.prepareStatement("update news_sys.customers set customer_phone = '" + newContent + "' where customer_id = '" + modifyCustId + "';");

			}
			else if(newChange == 3) {
				preparedStatement = connect.prepareStatement("update news_sys.customers set customer_pwd = '" + newContent + "' where customer_id = '" + modifyCustId + "';");
			}
			preparedStatement.executeUpdate();

		}
		catch (Exception e) {
			modifySucessfull = false;
		}
		return modifySucessfull;
	}
}


