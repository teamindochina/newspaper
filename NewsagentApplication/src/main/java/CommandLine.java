

import java.sql.ResultSet;
import java.util.Date;
import java.util.Scanner;

public class CommandLine {


	private static void listMainMenu() {
		System.out.println("Choose any one of the following");
		System.out.println("1. Manage customer(s)");
		System.out.println("2. Manage Subscription(s)");
		System.out.println("3. Manage Deliveries");
		System.out.println("4. Manage Payment(s)");
		System.out.println("5. Manage Reminder(s)");
	}
	
    private static void listSubscriptionMenu() {
    	System.out.println("Choose any one of the following");
		System.out.println("1. Create a subscription record");
		System.out.println("2. View all subscription records");
		System.out.println("3. Update a subscription record");
		System.out.println("4. Delete a subscription record");
		System.out.println("5. View the subscription count of an item");
		System.out.println("6. View all subscriptions of a customer");
		System.out.println("7. View a customer's subscriptions for a particular month");
	}

    private static void listDeliveryMenu() {
    	System.out.println("Choose any one of the following");
		System.out.println("1. Create a delivery record");
		System.out.println("2. View all delivery records");
		System.out.println("3. Delete a delivery record");
		System.out.println("4. List all customers in each area.");
		System.out.println("5. See customers to be delivered on a day.");
	
}

    private static void listPaymentMenu() {
    	System.out.println("Choose any one of the following");
		System.out.println("1. Create a payment record");
		System.out.println("2. View all payment record(s)");
		System.out.println("3. Update a payment record");
		System.out.println("4. Delete a payment record");
}
    
    private static void listPaymentReminderMenu() {
    	System.out.println("Choose any one of the following");
		System.out.println("1. Create a new payment reminder");
		System.out.println("2. View all payment reminder(s)");
		System.out.println("3. Update a payment reminder");
		System.out.println("4. Delete a payment reminder");
}

	private static boolean printDeliveryTable(ResultSet rs) throws Exception {
		
		
		
		System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
		System.out.println("Table: " + rs.getMetaData().getTableName(1));
		for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
			System.out.printf("%30s",rs.getMetaData().getColumnName(i));
		}
		System.out.println();
		while (rs.next()) {
			int customerID = rs.getInt("customer_id");
			String areaName = rs.getString("area_name");
			String addressName = rs.getString("address_name");
			String  postcode= rs.getString("postcode");
			int date = rs.getInt("delivery_date");
			System.out.printf("%30s", customerID);
			System.out.printf("%30s", areaName);
			System.out.printf("%30s", addressName);
			System.out.printf("%30s", postcode);
			System.out.printf("%30s", date);
			System.out.println();
		}// end while
		System.out.println("--------------------------------------------------------------------------------------------------------------------------------------");
		
		return true;
		
	}
	private static boolean printAreaCustomerTable(ResultSet rs) throws Exception {
		
		//Print The Contents of the Full Customer Table
		
		System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
		System.out.println("Table: " + rs.getMetaData().getTableName(1));
		for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
			System.out.printf("%30s",rs.getMetaData().getColumnName(i));
		}
		System.out.println();
		while (rs.next()) {
			String areaName = rs.getString("area_name");
			String CustomerId = rs.getString("customers");	
			System.out.printf("%30s", areaName);
			System.out.printf("%30s", CustomerId);
			System.out.println();
		}// end while
		System.out.println("--------------------------------------------------------------------------------------------------------------------------------------");
		
		return true;
		
	}

	private static boolean printPaymentTable(ResultSet rs) throws Exception {
		
		//Print The Contents of the Payment Table
		
		System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
		System.out.println("Table: " + rs.getMetaData().getTableName(1));
		for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
			System.out.printf("%30s",rs.getMetaData().getColumnName(i));
		}
		System.out.println();
		while (rs.next()) {
			int payID = rs.getInt("payment_id");
			String payMethod = rs.getString("payment_method");
			int custID = rs.getInt("customer_id");
			System.out.printf("%30s", payID);
			System.out.printf("%30s", payMethod);
			System.out.printf("%30s", custID);
			System.out.println();
		}// end while
		System.out.println("--------------------------------------------------------------------------------------------------------------------------------------");
		
		return true;
		
	}

	private static boolean printSubscriptionTable(ResultSet rs) throws Exception {
		
		//Print The Contents of the Full Customer Table
		
		System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
		System.out.println("Table: " + rs.getMetaData().getTableName(1));
		for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
			System.out.printf("%30s",rs.getMetaData().getColumnName(i));
		}
		System.out.println();
		while (rs.next()) {
			int subId = rs.getInt("sub_id");
			String subName = rs.getString("item_name");
			int subQty = rs.getInt("item_qty");
			int deliFreq = rs.getInt("delivery_frequency");
			int cusId = rs.getInt("customer_id");
			Date subDate = rs.getDate("sub_date");
			System.out.printf("%30s", subId);
			System.out.printf("%30s", subName);
			System.out.printf("%30s", subQty);
			System.out.printf("%30s", deliFreq);
			System.out.printf("%30s", cusId);
			System.out.printf("%30s", subDate);
			System.out.println();
		}// end while
		System.out.println("--------------------------------------------------------------------------------------------------------------------------------------");
		
		return true;
		
	}
	private static boolean printSubItemTableFor5(ResultSet rs) throws Exception {
		
		//Print The Contents of the Full Customer Table
		
		System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
		System.out.println("Table: " + rs.getMetaData().getTableName(1));
		for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
			System.out.printf("%30s",rs.getMetaData().getColumnName(i));
		}
		System.out.println();
		while (rs.next()) {
			String subName = rs.getString("item_name");
			int subCount = rs.getInt("count");
			System.out.printf("%30s", subName);
			System.out.printf("%30s", subCount);
			System.out.println();
		}// end while
		System.out.println("--------------------------------------------------------------------------------------------------------------------------------------");
		
		return true;
		
	}
	
    private static boolean printReminderTable(ResultSet rs) throws Exception {
		
		//Print The Contents of the Payment Table
		
		System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
		System.out.println("Table: " + rs.getMetaData().getTableName(1));
		for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
			System.out.printf("%30s",rs.getMetaData().getColumnName(i));
		}
		System.out.println();
		while (rs.next()) {
			int reminderID = rs.getInt("reminder_id");
			double dueAmount = rs.getDouble("amount_due");
			
			Date localDate = rs.getDate("date_due");
			String dueDate = localDate.toString();
			
			int custID = rs.getInt("customer_id");
			System.out.printf("%30s", reminderID);
			System.out.printf("%30s", dueAmount);
			System.out.printf("%30s", dueDate);
			System.out.printf("%30s", custID);
			System.out.println();
		}// end while
		System.out.println("--------------------------------------------------------------------------------------------------------------------------------------");
		
		return true;
		
	}
    
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			MySQLAccess dao = new MySQLAccess();
			
			int payID = 0;
			int reminderID = 0;
			String payMethod = "";
			double reminderAmount = 0;
			String reminderDate = "";
			Scanner keyboard = new Scanner(System.in);
			String taskNumber = "-99";
			String operationNumber = "-99";
			boolean keepAppOpen = true;
			
			while(keepAppOpen == true) {
				listMainMenu();
				taskNumber = keyboard.nextLine();  
				
				switch(taskNumber){
					case "1":
						CustomerOperation customerOperation = new CustomerOperation();
						keepAppOpen = customerOperation.performOperation(keyboard);
				// end switch
						break;
						
				// subscription
					case "2":
						listSubscriptionMenu();
						operationNumber = keyboard.nextLine();  
						switch(operationNumber) {
						case "1":
							//create a subscription record
							System.out.printf("Enter Subscription ID: \n");
							int subId = keyboard.nextInt();
							System.out.printf("Enter Subscription Name: \n");
							String subName = keyboard.next();
							System.out.printf("Enter Subscription Quantity: \n");
							int subQty = keyboard.nextInt();
							System.out.printf("Enter main.data.Delivery Frequency: \n");
							int deliFreq = keyboard.nextInt();
							System.out.printf("Enter Customer ID: \n");
							int cusId = keyboard.nextInt();
							System.out.printf("Enter Subscription Date: \n");
							String subDate = keyboard.next();
							
						
							SubRecord subObj = new SubRecord(subId,subName,subQty,deliFreq,cusId,subDate);
						
							//Insert sub Details into the database
							boolean insertResult = dao.insertSubscriptionDetails(subObj);
							if (insertResult == true)
								System.out.println("Subscription Details Saved");
							else 
								System.out.println("ERROR: Subscription Details NOT Saved");
							break;
							
						case "2": 
							//Retrieve ALL sub Records
							ResultSet rSet = dao.retrieveAllSubscriptions();
							if (rSet == null) {
								System.out.println("No Records Found");
								break;
							}
							else {
								boolean tablePrinted = printSubscriptionTable(rSet);
								if (tablePrinted == true)
									rSet.close();
							}
							break;
							
						case "3":
							//Update sub Record by ID
							System.out.println("Enter Subscription Id to be Updated");
							int modifySubId = keyboard.nextInt();
							System.out.println("Choose one of options to modify");
							System.out.println("1.item name");
							System.out.println("2.item qty");
							System.out.println("3.delivery frequency");
							int newChange = keyboard.nextInt();
							System.out.println("Enter new content to update");
							String newContent = keyboard.next();
							boolean modifyResult = dao.modifySubscriptinById(modifySubId, newChange, newContent);
							if (modifyResult == true)
								System.out.println("Subscription Table Updated");
							else 
								System.out.println("ERROR: Subscriptions NOT Updated or Do Not Exist");
							break;
						case "4":
							//Delete sub Record by ID
							System.out.println("Enter Subscription Id to be deleted or -99 to Clear all Rows");
							int deleteSubId = keyboard.nextInt();
							boolean deleteResult = dao.deleteSubscriptinById(deleteSubId);
							if ((deleteResult == true) && (deleteSubId==-99))
								System.out.println("Subscription Table Emptied");
							else if (deleteResult == true)
								System.out.println("Subscription Deleted");
							else 
								System.out.println("ERROR: Subscriptions NOT Deleted or Do Not Exist");
							break;
						case "5":
							//View sub Record by ID
							System.out.println("Enter Item name to view the sub count");
							String viewItemName = keyboard.nextLine();  
							ResultSet ResultItemNameSet = dao.retrieveSubCountByItemName(viewItemName);
							if (ResultItemNameSet == null) {
								System.out.println("No Records Found");
								break;
							}
							else {
								boolean tablePrinted = printSubItemTableFor5(ResultItemNameSet);
								if (tablePrinted == true)
									ResultItemNameSet.close();
							}
							break;
						case "6":
							//View sub Record by ID
							System.out.println("Enter customer Id to view the sub details");
							int viewCusId = Integer.valueOf(keyboard.nextLine());
							ResultSet ResultCusIdSet = dao.retrieveSubCountByCusId(viewCusId);
							if (ResultCusIdSet == null) {
								System.out.println("No Records Found");
								break;
							}
							else {
								boolean tablePrinted = printSubscriptionTable(ResultCusIdSet);
								if (tablePrinted == true)
									ResultCusIdSet.close();
							}
							break;
						case "7": 
							System.out.println("Enter customer Id");
							int checkCusId = Integer.valueOf(keyboard.nextLine());
							System.out.println("Enter month (please enter a number)");
							int checkMonth = Integer.valueOf(keyboard.nextLine());
							ResultSet ResultCusBillSet = dao.retrieveCusBill(checkCusId,checkMonth);
							if (ResultCusBillSet == null) {
								System.out.println("No Records Found");
								break;
							}
							else {
								boolean tablePrinted = printSubscriptionTable(ResultCusBillSet);
								if (tablePrinted == true)
									ResultCusBillSet.close();
							}
							break;
						}
						break;
						
					case "3":

						listDeliveryMenu();
						
						operationNumber = keyboard.next();
						
						switch(operationNumber) {
						case "1":
							boolean insertResult = false;
							try {
//							1. Create a delivery record (int id, String areaName, String streetName, int houseNumber)
							System.out.println("Enter customerId:");
							int deliveryID = keyboard.nextInt();
							System.out.println("Enter areaName:");
							String areaName = keyboard.next();
							System.out.println("Enter streetName:");
							String streetName = keyboard.next();
							System.out.println("Enter house number:");
							String houseNumber = keyboard.next();
							System.out.println("Enter deliverydate(1-30):");
							int deliverydate = keyboard.nextInt();
							Delivery d = new Delivery(deliveryID,areaName,streetName,houseNumber,deliverydate);
							insertResult = dao.insertNewDeliveryDetail(d);	}catch(Exception e) {
								System.out.println("Invalid type");
								break;
							}
							if(insertResult == true) 
								System.out.println("Delivery Details Entered!");
							else
								System.out.println("Please Try Again");
							break;
						case "2":
//							2. View all delivery records
							ResultSet rs = dao.displayAllDeliveryDetails();
							if(rs == null) {
								System.out.println("No records found");
								break;
							}
							else {
								boolean tablePrinted = printDeliveryTable(rs);
								if(tablePrinted == true)
									rs.close();
							}
							break;
							case "3":
								String deleteDelivery;
//						     3. Delete a delivery record
							try{System.out.println("Enter customer's id to delete its delivery details:");
							 deleteDelivery = keyboard.next();}catch(Exception e) {
								System.out.print("invalid type");
								break;
							}
							boolean deleteDelRes = dao.deleteDeliveryByID(Integer.parseInt(deleteDelivery));
							if (deleteDelRes == true)
								System.out.println("main.data.Delivery Record Deleted");
							else
								System.out.println("ERROR: Operation Unsuccessful");
							break;
						case "4":
//							4. List all customers in each area
							ResultSet cusArea = dao.displayCustomersForEachArea();
							if(cusArea== null) {
								System.out.println("No records found");
								break;
							}
							else {
								boolean tablePrinted = printAreaCustomerTable(cusArea);
								if(tablePrinted == true)
									cusArea.close();
							}
							break;
							case "5":
//							5.See customers to be delivered on a day 
							System.out.println("Enter delivery date you want(1-30):");
							int deliveryDate;
							try {
							String date1 = keyboard.next();
							deliveryDate = Integer.parseInt(date1);
							if(deliveryDate<1||deliveryDate>30) {
								throw new Exception();
								}}catch(Exception e) {
							
								System.out.print("Invalid type");
								break;
							}
							ResultSet rs1 = dao.displayitemstoday(deliveryDate);
							if(rs1 == null) {
								System.out.println("No records found");
								break;
							}
							else {
								boolean tablePrinted = printDeliveryTable(rs1);
								if(tablePrinted == true)
									rs1.close();
							}
							//See customers' items to delivery in a month.
							
						case "99":
							keepAppOpen = false;
							System.out.println("Closing the Application");
							break;
						default:
							System.out.println("invalid input");
							break;
						}
						break;
					case "4":
						listPaymentMenu();
						operationNumber = keyboard.next();
						switch(operationNumber) {
						case "1":
							System.out.println("Enter the Customer's ID: \n");
							String ID = keyboard.next();
							try {
								payID = Integer.parseInt(ID);

								System.out.println("Enter a Payment Method: \n");
								payMethod = keyboard.next();
								
								PaymentHandler p = new PaymentHandler(payID, payMethod);
								
								boolean insertResult = dao.insertNewPaymentDetail(p);
								
								if(insertResult == true) 
									System.out.println("Payment Details Entered!");
								else
									System.out.println("ERROR: Please Try Again");
							}catch(Exception e) {
								System.out.println(e.getMessage());
							}
							
							break;
						case "2":
							ResultSet rs = dao.displayAllPaymentDetails();
							if(rs == null) {
								System.out.println("No records found");
								break;
							}
							else {
								boolean tablePrinted = printPaymentTable(rs);
								if(tablePrinted == true)
									rs.close();
							}
							break;
						case "3":
							System.out.println("Enter the Customer's ID: \n");
							String toBeUpdatedID = keyboard.next();
							int toBeUpdatedPayID = Integer.parseInt(toBeUpdatedID);
							System.out.println("Enter a new Payment Method: \n");
							String toBeUpdatedPayMethod = keyboard.next(); 
							
							PaymentHandler pH = new PaymentHandler(toBeUpdatedPayID, toBeUpdatedPayMethod);
							
							boolean updateResult = dao.updateExistingPaymentDetail(pH);
							
							if(updateResult == true) 
								System.out.println("Payment Details Entered!");
							else
								System.out.println("ERROR: Please Try Again");
							break;
						case "4":
							System.out.println("Enter Payment ID to be deleted or -99 to Clear all Rows");
							String deletePayID = keyboard.next();
							
							boolean deletePayRes = dao.deletePaymentByID(Integer.parseInt(deletePayID));
							
							if((deletePayRes == true) && (deletePayID.equals("-99")))
								System.out.println("Payment Table Emptied");
							else if (deletePayRes == true)
								System.out.println("Payment Record Deleted");
							else
								System.out.println("ERROR: Operation Unsuccessful");
							break;
						case "99":
							keepAppOpen = false;
							System.out.println("Closing the Application");
							break;
						default:
							System.out.println("Invalid input!");
							break;
						}
						break;
					case "5":
						listPaymentReminderMenu();
						operationNumber = keyboard.next();
						switch(operationNumber) {
						case "1":
							try {
								System.out.println("Enter the Customer's ID: \n");
								String ID = keyboard.next();
								reminderID = Integer.parseInt(ID);

								System.out.println("Enter the due amount: \n");
								String Amount = keyboard.next();
								reminderAmount = Double.parseDouble(Amount);
								
								System.out.println("Enter the date due: \n");
								reminderDate = keyboard.next();
								
								PaymentReminder r = new PaymentReminder(reminderID, reminderAmount, reminderDate);
								
								boolean insertResult = dao.insertNewPaymentReminder(r);
								
								if(insertResult == true) 
									System.out.println("Payment Reminder Details Entered!");
								else
									System.out.println("ERROR: Please Try Again");
							}
							catch(Exception e) {
								System.out.println(e.getMessage());
							}
							
							break;
						case "2":
							ResultSet rs = dao.displayAllPaymentReminders();
							if(rs == null) {
								System.out.println("No records found");
								break;
							}
							else {
								boolean tablePrinted = printReminderTable(rs);
								if(tablePrinted == true)
									rs.close();
							}
							break;
						case "3":
							try {
							System.out.println("Enter the Customer's ID: \n");
							String toBeUpdatedID = keyboard.next();
							int toBeUpdatedPayID = Integer.parseInt(toBeUpdatedID);
							
							System.out.println("Enter a new due amount: \n");
							String toBeUpdatedAmount = keyboard.next(); 
							Double toBeUpdatedReminderAmount = Double.parseDouble(toBeUpdatedAmount);
							
							System.out.println("Enter a new due date: \n");
							String toBeUpdatedDate = keyboard.next();
							
							PaymentReminder r = new PaymentReminder(toBeUpdatedPayID, toBeUpdatedReminderAmount, toBeUpdatedDate);
							
							boolean updateResult = dao.updateExistingPaymentReminder(r);
							
							if(updateResult == true) 
								System.out.println("Payment Details Entered!");
							else
								System.out.println("ERROR: Please Try Again");
							break;
							}
							catch(Exception e) {
								System.out.println(e.getMessage());
							}
						case "4":
							try {
							System.out.println("Enter Payment ID to be deleted or -99 to Clear all Rows");
							String deleteReminderID = keyboard.next();
							
							boolean deletePayRes = dao.deletePaymentReminderByID(Integer.parseInt(deleteReminderID));
							
							if((deletePayRes == true) && (deleteReminderID.equals("-99")))
								System.out.println("Payment Table Emptied");
							else if (deletePayRes == true)
								System.out.println("Payment Record Deleted");
							else
								System.out.println("ERROR: Operation Unsuccessful");
							break;
							}
							catch(Exception e) {
								System.out.println(e.getMessage());
							}
						case "99":
							keepAppOpen = false;
							System.out.println("Closing the Application");
							break;
						default:
							System.out.println("Invalid input!");
							break;
						}
						break;
					default:
						System.out.println("Invalid input!");
						break;
				}
				keyboard.nextLine();
			}
			keyboard.close();
			
		}
		catch(Exception e) {
			System.out.println("PROGRAM TERMINATED - ERROR MESSAGE: " + e);
		}
	}

}
