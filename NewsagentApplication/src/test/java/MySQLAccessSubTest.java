
import java.sql.ResultSet;

import junit.framework.TestCase;

public class MySQLAccessSubTest extends TestCase {
	//Test No: 1
		//Objective: test connection
		
		public void testMySQLAccessConnection() {
			
			try {
				MySQLAccess dao = new MySQLAccess();
			} catch (Exception e) {
				fail("Connection not established");
			}
		}
		
		//Test No: 2
		//Objective: test insert method
		
		public void testInsertSub() {
			
			try {
				MySQLAccess testHelper = new MySQLAccess();
				SubRecord testObj = new SubRecord(3248754,"Jade",5,9,3,"2022-02-27");
				boolean result = testHelper.insertSubscriptionDetails(testObj);
				assertEquals(3248754, testObj.getSubId());
				assertEquals("Jade", testObj.getSubName());
				assertEquals(5, testObj.getSubQty());
				assertEquals(9, testObj.getDeliFreq());
				assertEquals(3, testObj.getCusId());
			} catch(Exception e) {
				fail("Exception not expected!");
			}
		}
		
		//Test No: 3
		//Objective: test display all records
		
		public void testDisplaySub() {
			
			try {
				MySQLAccess testHelper = new MySQLAccess();
				ResultSet testObj = testHelper.retrieveAllSubscriptions();
				if(testObj == null) {
					fail("No record(s) found");
				}
			} catch(Exception e) {
				fail("Exception not expected!");
			}
		}
		
		//Test No: 4
		// Objective: test delete
		
		public void testDeleteSub() {
			
			try {
				MySQLAccess testHelper = new MySQLAccess();
				
				boolean testObj = testHelper.deleteSubscriptinById(5);
				assertEquals(true, testObj);
			} catch(Exception e) {
				fail("Exception not expected!");
			}
		}
		
		//Test No: 5 6 7
		//Objective: test update itemName, itemQty, frequency
		
		public void testUpdateitemName() {
			
			try {
				MySQLAccess testHelper = new MySQLAccess();
				SubRecord testObj = new SubRecord(3248754,"Jado",5,9,3,"2022-02-27");
				boolean testResult = testHelper.modifySubscriptinById(3248754, 1, "Jade");
				assertEquals(true, testResult);
			}catch(Exception e) {
				fail("Exception not expected");
			}
		}
		
		public void testUpdateitemQty() {
			
			try {
				MySQLAccess testHelper = new MySQLAccess();
				SubRecord testObj = new SubRecord(3248754,"Jado",5,9,3,"2022-02-27");
				boolean testResult = testHelper.modifySubscriptinById(3248754, 2, "4");
				assertEquals(true, testResult);
			}catch(Exception e) {
				fail("Exception not expected");
			}
		}
		
		public void testUpdateitemFreq() {
			
			try {
				MySQLAccess testHelper = new MySQLAccess();
				SubRecord testObj = new SubRecord(3248754,"Jado",5,9,3,"2022-02-27");
				boolean testResult = testHelper.modifySubscriptinById(3248754, 3, "8");
				assertEquals(true, testResult);
			}catch(Exception e) {
				fail("Exception not expected");
			}
		}
		
		//Test No: 8
				//Objective: test retrieveSubCountByItemName method
				
		public void testRetrieveSubCountByItemName() {
					
			try {
				MySQLAccess testHelper = new MySQLAccess();
				ResultSet testObj = testHelper.retrieveSubCountByItemName("Irish Times");
				if(testObj == null) {
					fail("No record(s) found");
				}
			} catch(Exception e) {
				fail("Exception not expected!");
			}
		}
		//Test No: 9
		//Objective: test retrieveSubCountByCusId method
		
		public void testRetrieveSubCountByCusId() {
			
			try {
				MySQLAccess testHelper = new MySQLAccess();
				ResultSet testObj = testHelper.retrieveSubCountByCusId(1);
				if(testObj == null) {
					fail("No record(s) found");
				}
			} catch(Exception e) {
				fail("Exception not expected!");
			}
		}
		
		//Test No: 10
				//Objective: test retrieveCusBill method
				
		public void testRetrieveCusBill() {
					
			try {
				MySQLAccess testHelper = new MySQLAccess();
				ResultSet testObj = testHelper.retrieveCusBill(1, 1);
				if(testObj == null) {
				fail("No record(s) found");
				}
			} catch(Exception e) {
				fail("Exception not expected!");
			}
		}
				
}
