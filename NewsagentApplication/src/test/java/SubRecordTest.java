

import junit.framework.TestCase;

public class SubRecordTest extends TestCase  {
	// 10 tests
	public void testSet() throws SubRecordExceptionHandler {
		try {
			SubRecord testObj = new SubRecord(5248755,"Jado",4,8,2,"2022-02-26");
			testObj.setSubId(3248754);
			testObj.setSubName("Jade");
			testObj.setSubQty(5);
			testObj.setDeliFreq(9);
			testObj.setCusId(3);
			testObj.setSubDate("2022-02-27");
			assertEquals(3248754, testObj.getSubId());
			assertEquals("Jade", testObj.getSubName());
			assertEquals(5, testObj.getSubQty());
			assertEquals(9, testObj.getDeliFreq());
			assertEquals(3, testObj.getCusId());
			assertEquals("2022-02-27", testObj.getSubDate());
		}catch(SubRecordExceptionHandler e){
			fail("Exception not expected");
		}
	}
	public void testGetandConstructor() {
		try {
			SubRecord testObj = new SubRecord(3248754,"Jade",5,9,3,"2022-02-27");
			assertEquals(3248754, testObj.getSubId());
			assertEquals("Jade", testObj.getSubName());
			assertEquals(5, testObj.getSubQty());
			assertEquals(9, testObj.getDeliFreq());
			assertEquals(3, testObj.getCusId());
		}catch(SubRecordExceptionHandler e){
			fail("Exception not expected");
		}
		
	}
	public void testIdgetDescription001() {
		
		try {
			SubRecord.validateSubId(124);
			fail("Exception expected");
		}catch(SubRecordExceptionHandler e) {
			assertEquals("id less than 4 characters", e.getMessage());
		}
	
	}
	public void testIDgetDescription002() {
	
		try {
			SubRecord.validateSubId(124124124);
			fail("Exception expected");
		}catch(SubRecordExceptionHandler e) {
			assertEquals("id more than 8 characters", e.getMessage());
		}
		
	}
	public void testNamegetDescription001() {
		
		try {
			SubRecord.validateSubName("Ja");
			fail("Exception expected");
		}catch(SubRecordExceptionHandler e) {
			assertEquals("name less than 3 characters", e.getMessage());
		}

	}
	public void testNamegetDescription002() {
		
		try {
			SubRecord.validateSubName("JadeJadeJadeJadeJa");
			fail("Exception expected");
		}catch(SubRecordExceptionHandler e) {
			assertEquals("name more than 16 characters", e.getMessage());
		}
	}
	public void testNamegetDescription003() {
		
		try {
			SubRecord.validateSubName("Jade2022");
			fail("Exception expected");
		}catch(SubRecordExceptionHandler e) {
			assertEquals("name including number or other special characters", e.getMessage());
		}
	}
	public void testQtygetDescription001() {
		
		try {
			SubRecord.validateSubQty(-1);
			fail("Exception expected");
		}catch(SubRecordExceptionHandler e) {
			assertEquals("qty less than 0", e.getMessage());
		}
	}
	public void testFreqgetDescription001() {
		
		try {
			SubRecord.validateDeliFreq(0);
			fail("Exception expected");
		}catch(SubRecordExceptionHandler e) {
			assertEquals("freq less than 1", e.getMessage());
		}
	}
	public void testCusIdgetDescription001() {
		
		try {
			SubRecord.validateCusId(0);
			fail("Exception expected");
		}catch(SubRecordExceptionHandler e) {
			assertEquals("cusId less than 1", e.getMessage());
		}
	}

}
