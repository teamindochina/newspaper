
import junit.framework.TestCase;

public class DeliveryTest extends TestCase {

	//Test No: 0
	//Objective: Successfully instantiate Delivery object
	
		
	public void testDelivery() {
		try {
		new Delivery(3242,"Athlone","Westmeath","N37 FP89",5); }
		catch(Exception e) {
			fail("Delivery Object not create suitable.");
		}
	}
	//Test No: 1
	//Objective: Failed to instantiate MySQLAccess object
	public void testDelivery2() {
		
		try {
			new Delivery(3242,"A","Westmeath","N37 FP89",5);
		}catch(DeliveryFormatException e) {
			assertEquals("main.data.Delivery ID contains 3 to 6 valid characters between 0 to 9.",e.getMessage());
		}
	}
	//Test No: 2
	//Objective: get delivery date property from Object
	public void testGetDate() {
		Delivery delivery = new Delivery(3242,"Athlone","Westmeath","N37 FP89",5); 
		int date = delivery.getDate();
		assertEquals(date,5);
	}
//Test No: 3
	//Objective: change delivery date property successfully
	public void testSetDate() {
		Delivery delivery = new Delivery(3242,"Athlone","Westmeath","N37 FP89",5);
		delivery.setDate(7);
		int date = delivery.getDate();
		assertEquals(date,7);
	}
	//Test No: 4
	//Objective: get customerId successfully
	public void testGetId() {
		Delivery delivery = new Delivery(3242,"Athlone","Westmeath","N37 FP89",5);
		int id = delivery.getId();
		assertEquals(id,3242);
	}
	//Test No: 5
	//Objective: set customerId successfully
	public void testSetId() {
		Delivery delivery = new Delivery(3242,"Athlone","Westmeath","N37 FP89",5);
		delivery.setId(886);
		int id = delivery.getId();
		assertEquals(id,886);
	}
	//Test No: 6
	//Objective: get AreaName property from Delivery Object successfully
	public void testGetAreaName() {
		Delivery delivery = new Delivery(3242,"Athlone","Westmeath","N37 FP89",5);
		String areaName = delivery.getAreaName();
		assertEquals(areaName,"Athlone");
	}
	//Test No: 7
		//Objective: set AreaName property successfully
	public void testSetAreaName() {
		Delivery delivery = new Delivery(3242,"Athlone","Westmeath","N37 FP89",5);
		delivery.setAreaName("Dublin");
		String areaName = delivery.getAreaName();
		assertEquals(areaName,"Dublin");
	}
	//Test No: 8
		//Objective: get street name property from Delivery Object successfully
	public void testGetStreetName() {
		Delivery delivery = new Delivery(3242,"Athlone","Westmeath","N37 FP89",5);
		String streetName = delivery.getStreetName();
		assertEquals(streetName,"Westmeath");
	}
	//Test No: 9
			//Objective: set Street Name property successfully
	public void testSetStreetName() {
		Delivery delivery = new Delivery(3242,"Athlone","Westmeath","N37 FP89",5);
		delivery.setStreetName("changed");
		assertEquals(delivery.getStreetName(),"changed");
	}
	//Test No: 10
		//Objective: get house number property from Delivery Object successfully
	public void testGetHouseNumber() {
		Delivery delivery = new Delivery(3242,"Athlone","Westmeath","N37 FP89",5);
		assertEquals(delivery.getHouseNumber(),"N37 FP89");
	}
	//Test No: 11
			//Objective: set house number property successfully
	public void testSetHouseNumber() {
		Delivery delivery = new Delivery(3242,"Athlone","Westmeath","N37 FP89",5);
		delivery.setHouseNumber("N37 FP90");
		assertEquals(delivery.getHouseNumber(),"N37 FP90");
	}
	//Test No: 12
	//Objective: let input Id invalid(id<100 and id>999999)to check output Exception message
	public void testCheckId() {
		try {
			Delivery.checkId(55);
		}catch(DeliveryFormatException e) {
			assertEquals("Delivery ID contains 3 to 6 valid characters between 0 to 9.",e.getMessage());

		}
		try {
			Delivery.checkId(324213123);
		}catch(DeliveryFormatException e) {
			assertEquals("Delivery ID contains 3 to 6 valid characters between 0 to 9.",e.getMessage());

		}
	}
	//Test No: 13
		//Objective: let input delivery date invalid(id<1 and id>30)to check output Exception message
	public void testCheckDate() {
		try {
			Delivery.checkDate(55);
		}catch(DeliveryFormatException e) {
			assertEquals("Delivery date is invalid",e.getMessage());

		}
		try {
			Delivery.checkDate(0);
		}catch(DeliveryFormatException e) {
			assertEquals("Delivery date is invalid",e.getMessage());

		}
	}
	//Test No: 14
			//Objective: let address invalid to check output Exception message
	public void testCheckAddressName() {
		try {
			Delivery.checkAddressName("a");
		}catch(DeliveryFormatException e) {
			assertEquals("Area name contains 4 to 16 valid characters between a to z.",e.getMessage());

		}
	}

}
