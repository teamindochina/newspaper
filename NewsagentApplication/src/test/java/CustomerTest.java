

import junit.framework.TestCase;

public class CustomerTest extends TestCase {

    public void testCustomer001() {
        //Create the Customer Object
        try {
            //Call method under test
            Customer custObj = new Customer("Jack Daniels", "087-123123123", "Deepakkk");
            // Use getters to check for object creation
            assertEquals("Jack Daniels", custObj.getName());
            assertEquals("087-123123123", custObj.getPhone());
            assertEquals("Deepakkk", custObj.getPassword());
        } catch (CustomerExceptionHandler e) {
            fail("Exception not expected");
        }
    }
//2

    public void testValidatename001() {
        try {
            //Call method under test
            Customer.validatename("");
            fail("Exception expected");
        } catch (CustomerExceptionHandler e) {
            assertEquals("Customer Name NOT specified", e.getMessage());
        }
    }

    public void testValidatename002() {
        try {
            //Call method under test
            Customer.validatename("j");
            fail("Exception expected");
        } catch (CustomerExceptionHandler e) {
            assertEquals("Customer Name does not meet minimum length requirements", e.getMessage());
        }
    }

    public void testValidatename003() {
        try {
            //Call method under test
            Customer.validatename("jackychensumendhudeepakkanojiaquian");
            fail("Exception expected");
        } catch (CustomerExceptionHandler e) {
            assertEquals("Customer Name does not exceeds maximum length requirements", e.getMessage());
        }
    }

//3

    //4
    public void testValidatePhone001() {

        try {
            //Call method under test
            Customer.validatephone("");
            fail("Exception expected");
        } catch (CustomerExceptionHandler e) {
            assertEquals("Customer PhoneNumber NOT specified", e.getMessage());
        }
    }

    public void testValidatePhone002() {

        try {
            //Call method under test
            Customer.validatephone("89345");
            fail("Exception expected");
        } catch (CustomerExceptionHandler e) {
            assertEquals("Customer PhoneNumber does not meet minimum length requirements", e.getMessage());
        }
    }

    public void testValidatePhone003() {

        try {
            //Call method under test
            Customer.validatephone("89345123456789012");
            fail("Exception expected");
        } catch (CustomerExceptionHandler e) {
            assertEquals("Customer PhoneNumber does not exceeds maximum length requirements", e.getMessage());
        }
    }

    //5
    public void testValidatePassword001() {

        try {
            //Call method under test
            Customer.validatepassword("");
            fail("Exception expected");
        } catch (CustomerExceptionHandler e) {
            assertEquals("Customer Password NOT specified", e.getMessage());
        }
    }

    public void testValidatePassword002() {

        try {
            //Call method under test
            Customer.validatepassword("1233");
            fail("Exception expected");
        } catch (CustomerExceptionHandler e) {
            assertEquals("Customer Password does not meet minimum length requirements", e.getMessage());
        }
    }

    public void testValidatePassword003() {

        try {
            //Call method under test
            Customer.validatepassword("123312344567890qwertyuiop");
            fail("Exception expected");
        } catch (CustomerExceptionHandler e) {
            assertEquals("Customer Password does not exceeds maximum length requirements", e.getMessage());
        }
    }
//6

    public void testCustomerBoundryValue001() {

        //Create the Customer Object

        try {

            //Call method under test
            Customer custObj = new Customer( "Jack Dillon", "0892379557", "087-12312312");

            // Use getters to check for object creation
            assertEquals("Jack Dillon", custObj.getName());
            assertEquals("0892379557", custObj.getPhone());
            assertEquals("087-12312312", custObj.getPassword());

        } catch (CustomerExceptionHandler e) {
            fail("Exception not expected");
        }

    }

}
