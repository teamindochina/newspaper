import java.sql.ResultSet;

import junit.framework.TestCase;

public class MySQLAccessDeliveryTest extends TestCase {

	//Test No: 15
	//Objective:insert an delivery record to database successfully
	//and insert same record to get an fail
	public void testInsertNewDeliveryDetail() {
		Delivery d = new Delivery(88888,"sasad","adasd","dasdas",12);
		
		try {
			MySQLAccess dao = new MySQLAccess();
			boolean result = dao.insertNewDeliveryDetail(d);
			assertEquals(true,result);
			}catch(Exception e) {
				e.printStackTrace();
			}
		try {
			MySQLAccess dao1 = new MySQLAccess();
			boolean result = dao1.insertNewDeliveryDetail(d);
			assertEquals(false,result);
			}catch(Exception e) {
				e.printStackTrace();
			}
	}
	//Test No: 16
		//Objective:display all delivery details successfully
		
	public void testDisplayAllDeliveryDetails() {
		    try {
			MySQLAccess dao = new MySQLAccess();
			ResultSet result = dao.displayAllDeliveryDetails();
			}catch(Exception e){
				e.printStackTrace();
			}
			
	}
	//Test No: 17
			//Objective:delete an delivery details successfully
	public void testDeleteDeliveryByID() {
		try {
			MySQLAccess dao = new MySQLAccess();
			boolean result = dao.deleteDeliveryByID(131);
			assertEquals(true,result);
	}catch(Exception e) {
		fail("Can't delete.");
	}
		
	}
	//Test No: 18
	//Objective:display customers of each Area successfully
	public void testDisplayCustomersForEachArea() {
		 try {
				MySQLAccess dao = new MySQLAccess();
				dao.displayCustomersForEachArea();
				}catch(Exception e){
					fail();
				}
	}
	//Test No: 19
		//Objective:Successfully query daily orders
	public void testDisplayitemstoday() {
		try {
			MySQLAccess dao = new MySQLAccess();
			dao.displayitemstoday(3);
			}catch(Exception e){
				fail();
			}
	}

	}


